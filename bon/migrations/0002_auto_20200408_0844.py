# Generated by Django 3.0.5 on 2020-04-08 08:44

import bon.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bon', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='image',
            field=models.ImageField(blank=True, default=None, null=True, upload_to=bon.models.gallery_image_upload_to),
        ),
        migrations.AddField(
            model_name='gallery',
            name='image_small',
            field=models.ImageField(blank=True, default=None, null=True, upload_to=bon.models.gallery_image_small_upload_to),
        ),
    ]
