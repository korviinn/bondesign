# Generated by Django 3.0.4 on 2020-11-04 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bon', '0013_indexpage_main_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='rating',
            field=models.PositiveIntegerField(default=1, verbose_name='Нумерация отображения'),
        ),
    ]
