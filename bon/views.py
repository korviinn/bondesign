from typing import List, Optional

from django.forms import BooleanField, FileInput, Form, ImageField
from django.shortcuts import redirect
from django.views.generic import FormView, TemplateView

from .const import GALLERY_COLUMNS_COUNT
from .models import Contacts, DesignProject, Image, IndexPage, IndividualService, Partner, ProjectTutorial, \
    ProjectTutorialImage, \
    Service


class GalleryImageDto:
    def __init__(self, first_image: Optional[Image], second_image: Optional[Image], third_image: Optional[Image]):
        self.first_image = first_image
        self.second_image = second_image
        self.third_image = third_image

    @classmethod
    def load_gallery(cls, gallery_list: List[Image]):
        gallery_list.extend([None for _ in range(GALLERY_COLUMNS_COUNT - len(gallery_list))])
        return cls(first_image=gallery_list[0], second_image=gallery_list[1], third_image=gallery_list[2])


def split_images(galleries) -> List[GalleryImageDto]:
    gallery_dto_list = []
    for index in range(0, len(galleries), GALLERY_COLUMNS_COUNT):
        gallery_slice = galleries[index:index + GALLERY_COLUMNS_COUNT]
        gallery_dto_list.append(GalleryImageDto.load_gallery(gallery_slice))
    return gallery_dto_list


class BaseView(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        projects = DesignProject.objects.all().order_by('rating', '-id')
        kwargs['projects'] = projects
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class IndexView(BaseView):
    template_name = 'index.html'

    def get(self, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['index_page'] = IndexPage.objects.all()[0]
        return self.render_to_response(context)


class ProjectView(BaseView):
    template_name = 'project.html'

    def get(self, request, project_id: int, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        design_project = DesignProject.objects.get(id=project_id)
        images = Image.objects.filter(design_project=design_project).order_by('rating')
        gallery_dto_list = split_images(images)
        context['project'] = design_project
        context['gallery_dto_list'] = gallery_dto_list
        context['images'] = images
        return self.render_to_response(context)


class PriceServiceView(BaseView):
    template_name = 'price_service.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        services = Service.objects.all().order_by('-priority')
        context['services'] = services
        context['contacts'] = Contacts.objects.all()
        context['individual_services'] = IndividualService.objects.all()
        return self.render_to_response(context)


class ContactView(BaseView):
    template_name = 'contact.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['contacts'] = Contacts.objects.all()
        return self.render_to_response(context)


class PartnerView(BaseView):
    template_name = 'partner.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['partners'] = Partner.objects.all()
        return self.render_to_response(context)


# class NewsView(BaseView):
#     template_name = 'news.html'
#
#     def get(self, request, *args, **kwargs):
#         context = self.get_context_data(**kwargs)
#         context['events'] = Event.objects.all()
#         return self.render_to_response(context)

class ProjectTutorialView(ProjectView):
    template_name = 'project_tutorial.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        project_tutorial = ProjectTutorial.objects.first()
        images = ProjectTutorialImage.objects.filter(project_tutorial=project_tutorial).order_by('-is_plan', 'rating', 'id')
        gallery_dto_list = split_images(images)
        context['project'] = project_tutorial
        context['gallery_dto_list'] = gallery_dto_list
        context['images'] = images
        return self.render_to_response(context)


# class TestPageVIew(BaseView):
#     template_name = 'test_page.html'


class UploadImagesForm(Form):
    images = ImageField(widget=FileInput(attrs={'multiple': True}))


class UploadDesignImagesView(FormView):
    form_class = UploadImagesForm
    template_name = 'admin/upload_images.html'

    def post(self, request, *args, **kwargs):
        form = UploadImagesForm(request.FILES)
        for i in dict(form.data)['images']:
            Image.objects.create(design_project_id=kwargs['project_id'], image=i)
        return redirect(f'/admin/bon/designproject/{kwargs["project_id"]}/change')


class UploadProjectImagesForm(Form):
    images = ImageField(widget=FileInput(attrs={'multiple': True}))
    is_plan = BooleanField()


class UploadProjectTutorialView(FormView):
    form_class = UploadProjectImagesForm
    template_name = 'admin/upload_images.html'

    def post(self, request, *args, **kwargs):
        form = UploadImagesForm(request.FILES)
        if form.is_valid():
            print(form.cleaned_data)
        else:
            print('not valid')
            print(form.errors)
        for i in dict(form.data)['images']:
            ProjectTutorialImage.objects.create(project_tutorial_id=kwargs['project_id'], image=i)
        return redirect(f'/admin/bon/projecttutorial/{kwargs["project_id"]}/change')
