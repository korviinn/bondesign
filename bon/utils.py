import hashlib
import os
import re
from datetime import datetime


uniq_path_re = re.compile(r'#([YMDSe])')


def get_unique_name_for_file(prefix: str, filename: str, template: str = '#Y/#M/#S.#e'):
    assert not prefix.endswith('/')
    basename, ext = os.path.splitext(filename)
    now = datetime.now()
    repl = {
        '#Y': str(now.year),
        '#M': str(now.month),
        '#D': str(now.day),
        '#S': hashlib.md5('{}{}'.format(basename, str(now)).encode()).hexdigest(),
        '#e': ext.lstrip("."),
    }
    name = uniq_path_re.sub(lambda x: repl[x.group()], template)
    return f'{prefix}/{name}'
