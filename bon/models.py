from enum import Enum
from functools import lru_cache
from os import mkdir, path

from PIL import ExifTags, Image as PILImage
from django.conf import settings
from django.db.models import BooleanField, CASCADE, CharField, ForeignKey, ImageField, Model, PositiveIntegerField, \
    TextChoices, TextField

from bon.utils import get_unique_name_for_file


def gallery_image_upload_to(instance, filename):
    return get_unique_name_for_file('gallery/image', filename)


def gallery_image_small_upload_to(instance, filename):
    return get_unique_name_for_file('gallery/image_small', filename)


def event_upload_to(instance, filename):
    return get_unique_name_for_file('event/image', filename)


def partner_main_image_upload_to(instance, filename):
    return get_unique_name_for_file('partner/main_image', filename)


def index_page_main_image_upload_to(instance, filename):
    return get_unique_name_for_file('index_page/main_image', filename)


class IndexPage(Model):
    text = TextField(verbose_name='Текст на главной странице')
    main_image = ImageField(upload_to=index_page_main_image_upload_to, null=True, default=None, blank=True, verbose_name='Картинка на главной странице')

    class Meta:
        verbose_name = 'Текст на главной странице'
        verbose_name_plural = 'Текст на главной странице'

    def __str__(self):
        return 'Текст на главной странице'


class DesignProject(Model):
    title = CharField(max_length=128, null=True, blank=True, default=None, verbose_name='Название')
    description = TextField(verbose_name='Описание', null=True, blank=True, default=None,)
    main_image = ImageField(verbose_name='Заглавная картинка', null=True, blank=True, default=None,)
    rating = PositiveIntegerField(default=100, verbose_name='Нумерация отображения')

    class Meta:
        verbose_name = 'Дизайн-проект'
        verbose_name_plural = 'Дизайн-поекты'

    def __str__(self):
        return self.title


class Image(Model):
    design_project = ForeignKey(DesignProject, on_delete=CASCADE, verbose_name='Дизайн проект')
    image = ImageField(
        upload_to=gallery_image_upload_to,
        null=True,
        blank=True,
        default=None,
        verbose_name='Картинка большая'
    )
    image_small = ImageField(
        upload_to=gallery_image_small_upload_to,
        null=True,
        blank=True,
        default=None,
        verbose_name='Картинка маленькя'
    )
    rating = PositiveIntegerField(default=100, verbose_name='Нумерация отображения')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        if not self.image or self.image_small:
            return

        abs_path, rel_path = self.get_abs_and_rel_path()
        self.save_in_file_system(abs_path)
        self.image_small = rel_path
        super().save()

    def save_in_file_system(self, abs_path: str):
        image = PILImage.open(rf"{self.image.path}")
        try:
            exif = dict(image._getexif().items())
        except:
            exif = {}
        orientation_id = self.get_exif_orientation_id()
        exif_orientation = exif.get(orientation_id)

        if exif_orientation == 3:
            image = image.rotate(180, expand=True)
        elif exif_orientation == 6:
            image = image.rotate(270, expand=True)
        elif exif_orientation == 8:
            image = image.rotate(90, expand=True)

        x, y = image.size
        new_image = image.resize((int(x / 3), int(y / 3)))
        new_image.save(abs_path, quality=100)

    @lru_cache()
    def get_exif_orientation_id(self):
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                return orientation

    def get_abs_and_rel_path(self):
        img_name = self.image.path.split('/')[-1]
        rel_path = gallery_image_small_upload_to(self, img_name)
        abs_path = settings.MEDIA_ROOT + rel_path
        images_dir = '/'.join(abs_path.split('/')[:-1])
        self.create_folder_if_not_exists(images_dir)
        return abs_path, rel_path

    def create_folder_if_not_exists(self, dir_path: str) -> None:
        if not path.exists(dir_path):
            mkdir(dir_path)


class Contacts(Model):
    name = CharField(max_length=50, verbose_name='Имя')
    sername = CharField(max_length=50, verbose_name='Фамилия')
    email = CharField(max_length=50, verbose_name='Почта')
    phone = CharField(max_length=50, verbose_name='Телефон')

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'


class Partner(Model):
    title = CharField(max_length=255, verbose_name='Название')
    main_image = ImageField(upload_to=partner_main_image_upload_to, default=None, null=True, blank=True, verbose_name='Главная картинка')
    description = TextField(verbose_name='Описание', null=True, blank=True, default=None,)
    url = CharField(max_length=255, verbose_name='URL на страницу партнера')

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'


class Event(Model):
    title = CharField(max_length=255, verbose_name='Название события')
    description = TextField(verbose_name='Описание', null=True, blank=True, default=None,)
    additional_info = TextField(verbose_name='Дополнительная информация', null=True, blank=True, default=None,)
    image = ImageField(upload_to=event_upload_to, null=True, blank=True, default=None, verbose_name='Картинка')
    url = CharField(null=True, blank=True, default=None, max_length=255)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Service(Model):
    title = CharField(max_length=128, verbose_name='Название')
    description = TextField(verbose_name='Описание')
    additional_info = TextField(null=True, blank=True, default=None, verbose_name='Дополнительная информация')
    priority = PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Услуга и цена'
        verbose_name_plural = 'Услуги и цены'

    def __str__(self):
        return self.title


class PriceText(Model):
    price_text = TextField(max_length=400)
    service = ForeignKey(Service, related_name='prices_text', on_delete=CASCADE, verbose_name='Название пакета')


class Price(Model):
    price = PositiveIntegerField(default=0, verbose_name='Цена')
    meters_from = PositiveIntegerField(default=0, verbose_name='От кол-ва метров')
    meters_to = PositiveIntegerField(default=0, verbose_name='До кол-ва метров')
    service = ForeignKey(Service, related_name='prices', on_delete=CASCADE, verbose_name='Название пакета')

    class Meta:
        verbose_name = 'Цена'
        verbose_name_plural = 'Цены'

    def __str__(self):
        return f'{self.service.title} {self.price}'


class IndividualService(Model):
    text = TextField()

    class Meta:
        verbose_name = 'Отдельная услуга'
        verbose_name_plural = 'Отдельные услуги'

    def __str__(self):
        return self.text[:40]


class ProjectTutorial(Model):
    title = CharField(max_length=128, null=True, blank=True, default=None, verbose_name='Название')
    description = TextField(verbose_name='Описание', null=True, blank=True, default=None, )

    class Meta:
        verbose_name = 'Образец Дизайн-проекта'
        verbose_name_plural = 'Образец Дизайн-поекта'

    def __str__(self):
        return self.title


class ProjectTutorialImage(Model):
    project_tutorial = ForeignKey(ProjectTutorial, on_delete=CASCADE, verbose_name='Состав Дизайн проекта')
    image = ImageField(
        upload_to=gallery_image_upload_to,
        null=True,
        blank=True,
        default=None,
        verbose_name='Картинка большая'
    )
    image_small = ImageField(
        upload_to=gallery_image_small_upload_to,
        null=True,
        blank=True,
        default=None,
        verbose_name='Картинка маленькя'
    )
    rating = PositiveIntegerField(default=100, verbose_name='Нумерация отображения')
    is_plan = BooleanField(default=True, verbose_name='Чертеж')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        if not self.image or self.image_small:
            return

        abs_path, rel_path = self.get_abs_and_rel_path()
        self.save_in_file_system(abs_path)
        self.image_small = rel_path
        super().save()

    def save_in_file_system(self, abs_path: str):
        image = PILImage.open(rf"{self.image.path}")
        try:
            exif = dict(image._getexif().items())
        except:
            exif = {}
        orientation_id = self.get_exif_orientation_id()
        exif_orientation = exif.get(orientation_id)

        if exif_orientation == 3:
            image = image.rotate(180, expand=True)
        elif exif_orientation == 6:
            image = image.rotate(270, expand=True)
        elif exif_orientation == 8:
            image = image.rotate(90, expand=True)

        x, y = image.size
        new_image = image.resize((int(x / 3), int(y / 3)))
        new_image.save(abs_path, quality=100)

    @lru_cache()
    def get_exif_orientation_id(self):
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                return orientation

    def get_abs_and_rel_path(self):
        img_name = self.image.path.split('/')[-1]
        rel_path = gallery_image_small_upload_to(self, img_name)
        abs_path = settings.MEDIA_ROOT + rel_path
        images_dir = '/'.join(abs_path.split('/')[:-1])
        self.create_folder_if_not_exists(images_dir)
        return abs_path, rel_path

    def create_folder_if_not_exists(self, dir_path: str) -> None:
        if not path.exists(dir_path):
            mkdir(dir_path)
