from django.contrib.admin import ModelAdmin, StackedInline, register

from bon.models import Contacts, DesignProject, Event, Image, IndexPage, IndividualService, Partner, Price, \
    PriceText, ProjectTutorial, ProjectTutorialImage, Service


@register(IndexPage)
class IndexPageAdmin(ModelAdmin):
    # def has_add_permission(self, request):
    #     return False

    def has_delete_permission(self, request, obj=None):
        return False


class ImagesInline(StackedInline):
    model = Image
    extra = 0


@register(DesignProject)
class DesignProjectAdmin(ModelAdmin):
    change_form_template = 'admin/redirect_to_upload.html'
    list_display = ('title', )
    inlines = (ImagesInline,)

    def changelist_view(self, request, extra_context=None):
        context = {
            'custom_urls_data': [
                {'change_list_url': 'download_user_emails', 'change_list_title': "Выгрузить email ползователей"},
                {
                    'change_list_url': 'clean_once_opened_containers',
                    'change_list_title': "Удалить открытые, но не купленные контейнеры",
                },
            ]
        }
        return super().changelist_view(request, context)


class PriceInline(StackedInline):
    model = PriceText
    extra = 0


@register(Service)
class ServiceAdmin(ModelAdmin):
    inlines = (PriceInline,)


@register(Price)
class PriceAdmin(ModelAdmin):
    pass


@register(IndividualService)
class IndividualService(ModelAdmin):
    pass


@register(Contacts)
class ContactsAdmin(ModelAdmin):
    list_display = ('name', 'sername', 'email', 'phone')


@register(Partner)
class PartnerAdmin(ModelAdmin):
    list_display = ('title', 'description', 'url')


@register(Event)
class EventAdmin(ModelAdmin):
    list_display = ('title', )


class ProjectTutorialImagesInline(StackedInline):
    model = ProjectTutorialImage
    extra = 0


@register(ProjectTutorial)
class ProjectTutorialAdmin(ModelAdmin):
    list_display = ('title',)
    inlines = (ProjectTutorialImagesInline,)
    change_form_template = 'admin/redirect_to_upload_project_tutorial.html'

    # def has_add_permission(self, request):
    #     return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changelist_view(self, request, extra_context=None):
        context = {
            'custom_urls_data': [
                {'change_list_url': 'download_user_emails', 'change_list_title': "Выгрузить email ползователей"},
                {
                    'change_list_url': 'clean_once_opened_containers',
                    'change_list_title': "Удалить открытые, но не купленные контейнеры",
                },
            ]
        }
        return super().changelist_view(request, context)


from django.contrib import admin

admin.autodiscover()
admin.site.enable_nav_sidebar = False