from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from bon.views import ContactView, IndexView, ProjectTutorialView, PartnerView, PriceServiceView, ProjectView, \
    UploadDesignImagesView, UploadProjectTutorialView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path(
        'admin/bon/<int:project_id>/upload_design_images/',
        UploadDesignImagesView.as_view(),
        name='upload_design_images'
    ),
    path(
        'admin/bon/<int:project_id>/upload_project_tutorial_images/',
        UploadProjectTutorialView.as_view(),
        name='upload_project_tutorial_images'
    ),
    path('project/<int:project_id>/', ProjectView.as_view(), name='project'),
    path('price_service/', PriceServiceView.as_view(), name='price_service'),
    path('contacts/', ContactView.as_view(), name='contacts'),
    path('partners/', PartnerView.as_view(), name='partners'),
    path('project_tutorial/', ProjectTutorialView.as_view(), name='project_tutorial'),
    # path('test_page/', TestPageVIew.as_view(), name='test_page')
    path('admin/', admin.site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
